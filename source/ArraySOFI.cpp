#include "Defines.h"
#include "ParticleFinding.h"
#include "FileIO.h"
#include "SOFI.h"
#include "NewSOFI.h"
#include "ArraySOFI.h"

void ArraySOFI() {
    std::vector<int> ordersToCalculate = {2};

    SOFIOptions sofiOptions;
    sofiOptions.orders = ordersToCalculate;

    size_t xSize = 20;
    size_t ySize = 30;
    char data[xSize * ySize * 4 * 5] = {1, 2, 3, 4, 5, 6};
    
    std::shared_ptr<ImageLoader> imageLoader =
            std::shared_ptr<ImageLoader>(new ImageLoaderArray(xSize, ySize, 5, data));

    /*
      std::shared_ptr<ImageLoader> imageLoaderWrapper(new ImageLoaderWrapper(imageLoader));
      dynamic_cast<ImageLoaderWrapper*>(imageLoaderWrapper.get())->setImageRange(sofiOptions.nFramesToSkip, sofiOptions.nFramesToInclude);
      std::shared_ptr<ImageLoader> threadedImageLoaderWrapper(new BackgroundThreadImageLoaderWrapper(imageLoaderWrapper));
    */

    std::shared_ptr<ProgressReporter> progressReporter =
            std::shared_ptr<ProgressReporter>(new ProgressReporter_Silent());
		
    std::vector<ImagePtr> sofiOutputImages;
		
    DoNewSOFI(imageLoader, sofiOptions, progressReporter, sofiOutputImages);
        
    std::cout << "dims " << sofiOutputImages.size() << "\n";
}

/*
void ParseSOFIKeywordArguments(const mxArray** prhs, int nrhs, SOFIOptions& sofiOptions, bool& wantQuiet) {
    int firstKeywordIndex = 3;
    
	CheckKeywordAndArgumentTypes(prhs, nrhs, firstKeywordIndex);
    
    // most keyword arguments must be 1x1 matrices (scalars) of type double
    for (int i = firstKeywordIndex; i < nrhs; i += 2) {
        const std::string keyword = GetArrayString(prhs[i]);
        const mxArray* argument = prhs[i + 1];
        
        if (boost::iequals(keyword, "lagtimes") || boost::iequals(keyword, "pixelCombinationWeights")) {
            continue;   // not 1x1 in size
        }
        
        if ((mxGetNumberOfDimensions(argument) > 2) || (mxGetM(argument) != 1) || (mxGetN(argument) != 1)) {
            mexErrMsgTxt("all keyword arguments must be 1x1 matrices (scalar values)");
        }
    }
    
    // extract the values of the keyword arguments and perform more detailed error checking
    for (int i = firstKeywordIndex; i < nrhs; i += 2) {
        const std::string keyword = GetArrayString(prhs[i]);
        const mxArray* argument = prhs[i + 1];
        
        if (boost::iequals(keyword, "nFramesToSkip")) {
            if (*mxGetPr(argument) < 0.0) {
                mexErrMsgTxt("nFramesToSkip must be positive or zero");
            }
            sofiOptions.nFramesToSkip = *mxGetPr(argument);
        } else if (boost::iequals(keyword, "nFramesToInclude")) {
            sofiOptions.nFramesToInclude = *mxGetPr(argument);
        } else if (boost::iequals(keyword, "pixelationCorrection")) {
            sofiOptions.doPixelationCorrection = (*mxGetPr(argument) != 0.0);
        } else if (boost::iequals(keyword, "alsoCorrectVariance")) {
            sofiOptions.alsoCorrectVariance = (*mxGetPr(argument) != 0.0);
        } else if (boost::iequals(keyword, "batchSize")) {
            if (*mxGetPr(argument) <= 0.0) {
                mexErrMsgTxt("'batchSize' argument requires a positive non-zero value");
            }
            sofiOptions.batchSize = *mxGetPr(argument);
        } else if (boost::iequals(keyword, "pixelCombinationCutoff")) {
			sofiOptions.pixelCombinationCutoff = *mxGetPr(argument);
        } else if (boost::iequals(keyword, "jackknife")) {
            sofiOptions.wantJackKnife = (*mxGetPr(argument) != 0.0);
		} else if (boost::iequals(keyword, "lagtimes")) {
			if ((mxGetNumberOfDimensions(argument) > 2) || (mxGetN(argument) != 1)) {
				mexErrMsgTxt("lag times must be provided as a row vector");
			}
			double* lagPtr = mxGetPr(argument);
			for (size_t i = 0; i < mxGetM(argument); ++i) {
				sofiOptions.lagTimes.push_back(*lagPtr);
				lagPtr += 1;
			}
		} else if (boost::iequals(keyword, "allowSamePixels")) {
			if (*mxGetPr(argument) != 0.0) {
				sofiOptions.allowablePixelCombinations = SOFIOptions::AllowOverlappingPixels;
			} else {
				sofiOptions.allowablePixelCombinations = SOFIOptions::NonOverlappingPixels;
			}
        } else if (boost::iequals(keyword, "pixelCombinationWeights")) {
            if ((mxGetNumberOfDimensions(argument) > 2) || (mxGetN(argument) != 1)) {
                mexErrMsgTxt("pixel combination weights must be provided as a row vector");
            }
            double* weightsPtr = mxGetPr(argument);
            for (size_t i = 0; i < mxGetM(argument); ++i) {
                sofiOptions.pixelCombinationWeights.push_back(*weightsPtr);
                weightsPtr += 1;
            }
        } else if (boost::iequals(keyword, "quiet")) {
            wantQuiet = (*mxGetPr(argument) != 0.0);
        } else {
			mexPrintf("warning - ignored unknown keyword \"%s\"\n", keyword.c_str());
		}
        
    }
}
*/
